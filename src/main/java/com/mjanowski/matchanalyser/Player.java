package com.mjanowski.matchanalyser;

import java.util.List;
import java.util.ArrayList;

class Player {

    protected String uID;
    protected String firstName;
    protected String lastName;
    protected String position;
    protected Team team;
    protected List<PlayerStatistic> statistics;

    public Player() {
        this.statistics = new ArrayList<PlayerStatistic>();
    }

    public String getuID() {
        return uID;
    }

    public void setuID(String uID) {
        this.uID = uID;
    }

    public String getFirstName() {
        return firstName;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    public String getLastName() {
        return lastName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    public String getPosition() {
        return position;
    }

    public void setPosition(String position) {
        this.position = position;
    }

    public Team getTeam() {
        return team;
    }

    public void setTeam(Team team) {
        this.team = team;
        this.team.addPlayer(this);
    }

    public List<PlayerStatistic>getStatistics() {
        return this.statistics;
    }

    public void addStatistic(PlayerStatistic playerStatistic) {
        this.statistics.add(playerStatistic);
    }

    @Override
    public String toString() {
        return this.getFirstName() + " " + this.getLastName();
    }

}
