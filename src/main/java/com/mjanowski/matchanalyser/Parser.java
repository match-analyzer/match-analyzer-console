package com.mjanowski.matchanalyser;

import java.util.*;
import org.w3c.dom.*;
import javax.xml.parsers.*;
import java.io.*;
import javax.xml.XMLConstants;
import javax.xml.validation.SchemaFactory;
import javax.xml.validation.Schema;
import javax.xml.validation.Validator;
import javax.xml.transform.dom.DOMSource;


class Parser {

    protected String pathName;
    protected HashMap<String, Player> playersMap;
    protected HashMap<String, Team> teamsMap;
    protected HashMap<String, TreeMap<Integer, PlayerStatistic>> playerStatisticsMap;
    protected HashMap<String, HashMap<String, Integer>> teamStatisticsMap;

    /**
     * Number of players displayed in ranking
     */
    static final int RANKING_PLAYERS_NUMBER = 5;

    public Parser(String pathName) {
        this.pathName = pathName;
        this.playersMap = new HashMap<String, Player>();
        this.teamsMap = new HashMap<String, Team>();
        this.playerStatisticsMap = new HashMap<String, TreeMap<Integer, PlayerStatistic>>();
        this.teamStatisticsMap = new HashMap<String, HashMap<String, Integer>>();
    }

    public void showStatistics(String statisticType) throws Exception {
        DocumentBuilderFactory factory = DocumentBuilderFactory.newInstance();
        DocumentBuilder builder = factory.newDocumentBuilder();
        Document document = builder.parse(new File( this.pathName ));

        //Given file does not have any style information associated with it.
        // That's why I'm skipping validation here...
//        this.validateFile(document);

        document.getDocumentElement().normalize();

        NodeList teamNodes = document.getElementsByTagName("Team");
        NodeList statisticNodes = document.getElementsByTagName("TeamData");

        this.loadTeamsWithPlayers(teamNodes);
        this.loadStatistics(statisticNodes);
        this.validateStatType(statisticType);
        this.printRanking(statisticType);
    }

    protected void validateFile(Document document) throws Exception {
        String language = XMLConstants.W3C_XML_SCHEMA_NS_URI;
        SchemaFactory factory = SchemaFactory.newInstance(language);
        Schema schema = factory.newSchema(new File(this.pathName));
        Validator validator = schema.newValidator();
        validator.validate(new DOMSource(document));
    }

    protected void loadTeamsWithPlayers(NodeList nodes) throws Exception {

        int counter = 0;

        while (counter < nodes.getLength()) {
            Node node = nodes.item(counter);
            if(node.getNodeType() != Node.ELEMENT_NODE) throw new Exception("Wrong file structure!");

            Element element = (Element) node;

            String teamId = element.getAttribute("uID");
            String country = element.getElementsByTagName("Country").item(0).getTextContent();
            String name = element.getElementsByTagName("Name").item(0).getTextContent();
            NodeList players = element.getElementsByTagName("Player");

            Team team = new Team();
            team.setuID(teamId);
            team.setCountry(country);
            team.setName(name);

            this.loadPlayers(players, team);

            this.teamsMap.put(teamId, team);
            counter++;
        }
    }

    protected void loadPlayers(NodeList nodes, Team team) throws Exception {

        int counter = 0;

        while (counter < nodes.getLength()) {
            Node node = nodes.item(counter);
            if(node.getNodeType() != Node.ELEMENT_NODE) throw new Exception("Wrong file structure!");

            Element element = (Element) node;
            Element personNameEl = (Element) element.getElementsByTagName("PersonName").item(0);
            Player player = new Player();
            String playerId = element.getAttribute("uID");

            player.setuID(playerId);
            player.setFirstName(personNameEl.getElementsByTagName("First").item(0).getTextContent());
            player.setLastName(personNameEl.getElementsByTagName("Last").item(0).getTextContent());
            player.setPosition(element.getAttribute("Position"));
            player.setTeam(team);

            this.playersMap.put(playerId, player);
            counter++;
        }

    }



    protected void loadStatistics(NodeList nodes) throws Exception {
        int counter = 0;

        while (counter < nodes.getLength()) {
            Node node = nodes.item(counter);
            if(node.getNodeType() != Node.ELEMENT_NODE) throw new Exception("Wrong file structure!");

            Element element = (Element) node;

            String teamRef = element.getAttribute("TeamRef");
            String teamSide = element.getAttribute("Side");
            NodeList playersStatistics = element.getElementsByTagName("MatchPlayer");

            Team currentTeam = this.teamsMap.get(teamRef);
            currentTeam.setSide(teamSide);

            this.loadPlayersStatistics(playersStatistics, currentTeam);
            counter++;
        }
    }


    protected void loadPlayersStatistics(NodeList nodes, Team team) throws Exception{
        int counter = 0;

        while (counter < nodes.getLength()) {

            Node node = nodes.item(counter);
            if(node.getNodeType() != Node.ELEMENT_NODE) throw new Exception("Wrong file structure!");

            Element element = (Element) node;

            String playerRef = element.getAttribute("PlayerRef");
            NodeList stats = element.getElementsByTagName("Stat");
            Player currentPlayer = this.playersMap.get(playerRef);

            for (int i=0; i<stats.getLength(); i++) {
                Node stat = stats.item(i);
                if(stat.getNodeType() != Node.ELEMENT_NODE) throw new Exception("Wrong file structure!");
                Element elStat = (Element) stat;

                String statType = elStat.getAttribute("Type");
                Integer statValue = Integer.parseInt(elStat.getTextContent());

                PlayerStatistic playerStatistic = new PlayerStatistic();
                playerStatistic.setName(statType);
                playerStatistic.setValue(statValue);
                playerStatistic.setPlayer(currentPlayer);

                this.addSinglePlayerStatistic(playerStatistic);
                this.addSingleTeamStatistic(playerStatistic);
            }

            counter++;
        }
    }

    protected void addSinglePlayerStatistic(PlayerStatistic statistic) {

        String statName = statistic.getName();
        Integer statValue = statistic.getValue();

        TreeMap<Integer, PlayerStatistic> statPlayersRanking = this.playerStatisticsMap.get(statName);

        if(statPlayersRanking != null) {
            statPlayersRanking.put(statValue, statistic);
        } else {
            TreeMap<Integer, PlayerStatistic> newStatPlayerRanking = new TreeMap<Integer, PlayerStatistic>(Collections.reverseOrder());
            newStatPlayerRanking.put(statValue, statistic);
            this.playerStatisticsMap.put(statName, newStatPlayerRanking);
        }
    }

    protected void addSingleTeamStatistic(PlayerStatistic statistic) {

        String teamId = statistic.getPlayer().getTeam().getuID();
        String statName = statistic.getName();
        Integer statValue = statistic.getValue();

        HashMap<String, Integer> teamStatsTotal = this.teamStatisticsMap.get(teamId);

        if(teamStatsTotal != null) {
            Integer oldValue = teamStatsTotal.get(statName);
            Integer newValue = (oldValue != null) ? (oldValue + statValue) : statValue;
            teamStatsTotal.put(statName, newValue);
        } else {
            HashMap<String, Integer> newTeamStatsTotal = new HashMap<String, Integer>();
            newTeamStatsTotal.put(statName, statValue);
            this.teamStatisticsMap.put(teamId, newTeamStatsTotal);
        }

    }

    protected void validateStatType(String statType) throws Exception {
        if(this.playerStatisticsMap.get(statType) == null) throw new Exception("Given type of statistic does not exists!");
    }

    protected void printRanking(String statisticType) {

        System.out.println("\n======== Top "+ RANKING_PLAYERS_NUMBER +" players ========");
        TreeMap<Integer, PlayerStatistic> statisticRanking = this.playerStatisticsMap.get(statisticType);

        int rankPos = 1;
        for(Map.Entry<Integer, PlayerStatistic> entry : statisticRanking.entrySet()) {
            if(rankPos > RANKING_PLAYERS_NUMBER) break;
            PlayerStatistic playerStat = entry.getValue();
            System.out.println(rankPos + ". " +playerStat);
            rankPos++;
        }

        for(int i=0; i<RANKING_PLAYERS_NUMBER; i++) {
            statisticRanking.descendingMap();
        }

        System.out.println("\n======== Teams overall ========");

        for(Map.Entry<String, HashMap<String, Integer>> entry : this.teamStatisticsMap.entrySet()) {
            String refTeam = entry.getKey();
            HashMap<String, Integer> totalRank = entry.getValue();
            Team team = this.teamsMap.get(refTeam);
            System.out.println(team + " - " + totalRank.get(statisticType));
        }
    }

}
