package com.mjanowski.matchanalyser;

class PlayerStatistic {

    protected String name;
    protected Integer value;
    protected Player player;

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Integer getValue() {
        return value;
    }

    public void setValue(Integer value) {
        this.value = value;
    }

    public Player getPlayer() {
        return player;
    }

    public void setPlayer(Player player) {
        this.player = player;
        this.player.addStatistic(this);
    }

    @Override
    public String toString() {
        return this.getPlayer() + " - " + this.getValue();
    }
}