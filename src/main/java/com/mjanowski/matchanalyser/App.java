package com.mjanowski.matchanalyser;

public class App {

    public static void main(String[] args) {
        try {
            if(args.length < 2) {
                throw new Exception("Missing parameters. Please provide type of statistic and path to file.");
            }

            String statisticType = args[0];
            String pathName = args[1];
            Parser parser = new Parser(pathName);

            parser.showStatistics(statisticType);

        } catch(Throwable e) {
            System.out.println(e.getMessage());
        }
    }
}
