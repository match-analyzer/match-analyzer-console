package com.mjanowski.matchanalyser;

import java.util.List;
import java.util.ArrayList;

class Team {

    protected String uID;
    protected String name;
    protected String country;
    protected String side;
    protected ArrayList<Player> players;

    public Team() {
        this.players = new ArrayList<Player>();
    }

    public String getuID() {
        return this.uID;
    }

    public void setuID(String uID) {
        this.uID = uID;
    }

    public String getName() {
        return this.name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getSide() {
        return this.side;
    }

    public void setSide(String side) {
        this.side = side;
    }

    public String getCountry() {
        return country;
    }

    public void setCountry(String country) {
        this.country = country;
    }

    public List<Player> getPlayers() {
        return this.players;
    }

    public void addPlayer(Player player) {
        this.players.add(player);
    }

    @Override
    public String toString() {
        return this.getSide() + "; " + this.getName();
    }
}
