# Match analyser console

App parses match document and generates players ranking
## Prerequirements
Before you build & run the project you have to make sure that you have `JDK` and `Maven` installed.

## Build
Run `mvn package` to build project 

## Run
`java -jar ./target/matchAnalyser-1.0-SNAPSHOT.jar <statisticType> <pathToXMLFile>`

